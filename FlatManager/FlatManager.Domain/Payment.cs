﻿using FlatManager.Domain.Enums;
using SharedKernel.Data;
using System;

namespace FlatManager.Domain
{
    public class Payment : BaseEntity
    {
        public int PaymentId { get; set; }
        public decimal Cost { get; set; }
        public DateTime DateOfPayment { get; set; }
        public PaymentType PaymentType { get; set; }

        public Company Company { get; set; }
    }
}
