﻿using FlatManager.Domain.Enums;
using SharedKernel.Data;
using System;

namespace FlatManager.Domain
{
    public class PayedBill : BaseEntity
    {
        public DateTime DateOfPay { get; set; }
        public MonthType MonthOfPayment { get; set; }
        public decimal Cost { get; set; }
        public decimal PriceFromPayment { get; set; }
        public Payment Payment { get; set; }
    }
}
