﻿using FlatManager.Domain.Enums;
using SharedKernel.Data;

namespace FlatManager.Domain
{
    public class Company : BaseEntity
    {
        public string Name { get; set; }
        public CompanyType CompanyType { get; set; }
    }
}
