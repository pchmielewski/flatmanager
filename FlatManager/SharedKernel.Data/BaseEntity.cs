﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharedKernel.Data
{
    public abstract class BaseEntity : IEntity
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }

        public BaseEntity()
        {
            Created = DateTime.Now;
        }
    }
}
