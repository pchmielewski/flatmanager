﻿using System;

namespace SharedKernel.Data
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}