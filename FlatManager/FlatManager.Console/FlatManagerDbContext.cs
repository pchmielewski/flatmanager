﻿using FlatManager.Domain;
using Microsoft.EntityFrameworkCore;

namespace FlatManager.Console
{
    public class FlatManagerDbContext : DbContext
    {
        //reszyta 
        public DbSet<Company> Companies { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PayedBill> PayedBills { get; set; }

        public FlatManagerDbContext()
        {

        }

        public FlatManagerDbContext(DbContextOptions<FlatManagerDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=MyDatabase;Trusted_Connection=True;");
        }
    }
}
