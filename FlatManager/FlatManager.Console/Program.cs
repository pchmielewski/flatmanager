﻿using FlatManager.Data;
using FlatManager.Domain;
using FlatManager.Domain.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlatManager.Console
{
    public class Program
    {
        // https://docs.efproject.net/en/latest/platforms/netcore/new-db-sqlite.html
        public static void Main(string[] args)
        {
            using (var db = new FlatManagerDbContext())
            {
                db.Companies.Add(new Company { CompanyType = CompanyType.FlatBils, Name = "Spółdzielnia" });
                db.SaveChanges();
                System.Console.WriteLine("Company" + db.Companies.FirstOrDefault().Name);
                System.Console.ReadLine();
            }
        }
    }
}
