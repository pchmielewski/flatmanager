﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using FlatManager.Console;

namespace FlatManager.Console.Migrations
{
    [DbContext(typeof(FlatManagerDbContext))]
    partial class FlatManagerDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("FlatManager.Domain.Company", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CompanyType");

                    b.Property<DateTime>("Created");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Companies");
                });

            modelBuilder.Entity("FlatManager.Domain.PayedBill", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Cost");

                    b.Property<DateTime>("Created");

                    b.Property<DateTime>("DateOfPay");

                    b.Property<int>("MonthOfPayment");

                    b.Property<int?>("PaymentId");

                    b.Property<decimal>("PriceFromPayment");

                    b.HasKey("Id");

                    b.HasIndex("PaymentId");

                    b.ToTable("PayedBills");
                });

            modelBuilder.Entity("FlatManager.Domain.Payment", b =>
                {
                    b.Property<int>("PaymentId")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("CompanyId");

                    b.Property<decimal>("Cost");

                    b.Property<DateTime>("Created");

                    b.Property<DateTime>("DateOfPayment");

                    b.Property<Guid>("Id");

                    b.Property<int>("PaymentType");

                    b.HasKey("PaymentId");

                    b.HasIndex("CompanyId");

                    b.ToTable("Payments");
                });

            modelBuilder.Entity("FlatManager.Domain.PayedBill", b =>
                {
                    b.HasOne("FlatManager.Domain.Payment", "Payment")
                        .WithMany()
                        .HasForeignKey("PaymentId");
                });

            modelBuilder.Entity("FlatManager.Domain.Payment", b =>
                {
                    b.HasOne("FlatManager.Domain.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");
                });
        }
    }
}
